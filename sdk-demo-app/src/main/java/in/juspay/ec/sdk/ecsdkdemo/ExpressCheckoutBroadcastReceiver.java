package in.juspay.ec.sdk.ecsdkdemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import in.juspay.ec.sdk.checkout.GodelCheckout;

public class ExpressCheckoutBroadcastReceiver extends BroadcastReceiver {
    public ExpressCheckoutBroadcastReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();
        String event = intent.getStringExtra("event");

        if (action.equals(GodelCheckout.EXPRESS_CHECKOUT_INTENT_ACTION)) {
            if (event.equals("endUrlReached")) {
                String url = intent.getStringExtra("url");
                Toast.makeText(context, "End URL Reached - [" + url+"]", Toast.LENGTH_LONG).show();
            }

            if (event.equals("transactionAborted")) {
                Toast.makeText(context,"Transaction Aborted",Toast.LENGTH_SHORT).show();
            }
        }
    }
}
