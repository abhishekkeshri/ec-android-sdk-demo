package in.juspay.ec.sdk.ecsdkdemo;

import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import in.juspay.ec.sdk.activities.ExpressCheckoutActivity;
import in.juspay.ec.sdk.activities.ui.OrderSummary;
import in.juspay.ec.sdk.api.Environment;
import in.juspay.ec.sdk.api.ExpressCheckoutService;
import in.juspay.ec.sdk.api.PaymentInstrument;
import in.juspay.ec.sdk.api.core.WalletPayment;
import in.juspay.ec.sdk.checkout.MobileWebCheckout;
import in.juspay.ec.sdk.netutils.JuspayHttpResponse;
import in.juspay.ec.sdk.netutils.NetUtils;
import in.juspay.juspaysafe.BrowserCallback;
import in.juspay.juspaysafe.BrowserParams;
import in.juspay.juspaysafe.JuspaySafeBrowser;

//import in.juspay.ec.sdk.activities.CheckoutActivity;
//import in.juspay.ec.sdk.activities.ActivityAwareBrowserCallback;

public class DemoActivity extends AppCompatActivity {
    private static final String TAG = "DemoActivity";

    ExecutorService executorService = Executors.newSingleThreadExecutor();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_demo);

        Typeface face = Typeface.createFromAsset(getAssets(), "SoftLine.otf");

        ((TextView) findViewById(R.id.webButton)).setTypeface(face);
        ((TextView) findViewById(R.id.nativeButton)).setTypeface(face);

        ((LinearLayout) findViewById(R.id.webButton).getParent()).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMobileWeb();
            }
        });

        ((LinearLayout) findViewById(R.id.nativeButton).getParent()).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                executorService.execute(new Runnable() {
//                    @Override
//                    public void run() {
//                        testOlaMoney();
//                    }
//                });
                startNative();
            }
        });
        startNative();
    }


    private void startNative() {
        Environment.configure(Environment.SANDBOX, "sriduth_sandbox_test");

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(final Void... params) {
                return generateOrder();
            }

            @Override
            protected void onPostExecute(final String s) {
                super.onPostExecute(s);
                ExpressCheckoutActivity.startPaymentActivity(
                        s,
                        s,
                        EnumSet.of(PaymentInstrument.CARD, PaymentInstrument.NB),
                        new BrowserCallback() {
                            @Override
                            public void endUrlReached(final JSONObject jsonObject) {
                                Log.d(TAG, "endUrlReached() called with: s = [" + s + "]");

                            }

                            @Override
                            public void onTransactionAborted(final JSONObject jsonObject) {
                                Log.d(TAG, "ontransactionAborted() called");

                            }
                        }, new String[]{".*https://sandbox.juspay.in/vbv/mc_secure.*"},
                        new OrderSummary.DefaultOrderSummaryBuilder("123", "INR 300", "JUSPAY Express Checkout", "Open").build(),
                        DemoActivity.this);
            }
        }.execute();


    }

    private void testOlaMoney() {
        WalletPayment payment = new WalletPayment();
        try {
            payment.setEndUrlRegexes(new String[]{".*localhost.*"})
                    .setMerchantId("sriduth_test")
                    .setWallet("OLAMONEY")
                    .setOrderId(generateOrder())
                    .startPayment(DemoActivity.this,

                            new BrowserCallback() {
                                @Override
                                public void endUrlReached(final String s) {

                                }

                                @Override
                                public void endUrlReached(final JSONObject jsonObject) {

                                }

                                @Override
                                public void ontransactionAborted() {

                                }

                                @Override
                                public void onTransactionAborted(final JSONObject jsonObject) {

                                }
                            }, new ExpressCheckoutService.TxnInitListener() {
                                @Override
                                public void beforeInit() {

                                }

                                @Override
                                public void onTxnInitResponse(final ExpressCheckoutService.ExpressCheckoutResponse initResponse) {

                                }

                                @Override
                                public void initError(final Exception exception, @Nullable final ExpressCheckoutService.ExpressCheckoutResponse expressCheckoutResponse) {

                                }
                            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startMobileWeb() {
//        executorService.execute(new Runnable() {
//            @Override
//            public void run() {
//                testOlaMoney();
//            }
//        });


        Environment.configure(Environment.SANDBOX, "sriduth_sandbox_test");

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(final Void... params) {
                return generateOrder();
            }

            @Override
            protected void onPostExecute(final String s) {
                super.onPostExecute(s);
                MobileWebCheckout checkout = new MobileWebCheckout(
                        s,
                        new String[]{".*google.*"},
                        new PaymentInstrument[]{PaymentInstrument.CARD, PaymentInstrument.WALLET, PaymentInstrument.NB}
                );


                checkout.startPayment(DemoActivity.this, new BrowserParams(), new BrowserCallback() {
                    @Override
                    public void endUrlReached(String s) {
                    }

                    @Override
                    public void endUrlReached(final JSONObject jsonObject) {

                    }

                    @Override
                    public void ontransactionAborted() {
                    }

                    @Override
                    public void onTransactionAborted(final JSONObject jsonObject) {

                    }
                });
            }
        }.execute();


    }

    public String generateOrder() {
        String orderId = null;
        try {
            Log.d(TAG, "generateOrder() called");
            orderId = Long.toString(System.currentTimeMillis());
            HttpURLConnection connection = (HttpURLConnection) Environment.getUrl("/order/create").openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Authorization", "Basic RkI4OUMwNkFBM0Y3NDY5MkI5NDA2QTczODBGRjQyMjg=");
            connection.setDoOutput(true);
            Map<String, String> payload = new HashMap<>();
            payload.put("order_id", orderId);
            payload.put("amount", "1.00");

            OutputStream stream = connection.getOutputStream();
            stream.write(NetUtils.generateQueryString(payload).getBytes());

            JuspayHttpResponse response = new JuspayHttpResponse(connection);
            JSONObject responseJson = new JSONObject(response.responsePayload);

            System.out.println(String.format(
                    "/order/create: \n%s", responseJson.toString()
            ));

            if (response.responseCode == 200) {
                return orderId;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return orderId;
    }
}
